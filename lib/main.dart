import 'package:flutter/material.dart';
import 'userInterface.dart/splash_screen.dart';

void main() {
  runApp(SakhumziLoginApp());
}

class SakhumziLoginApp extends StatelessWidget {
  SakhumziLoginApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Sakhumzi Login',
      theme: ThemeData(
        primaryColor: Colors.redAccent,
        scaffoldBackgroundColor: Colors.grey.shade100,
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.grey)
            .copyWith(secondary: Colors.redAccent),
      ),
      home: SplashScreen(title: 'Sakhumzi Login'),
    );
  }
}
